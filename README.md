WE-Module3: Generative AI Coursework

This repository hosts the coursework and code for the Generative AI classes undertaken as part of Module 3 in the Women Engineers program, facilitated by Talentsprint and supported by Google.

Accomplishments:

1.Resolved a computational thinking problem leveraging GenAI capabilities.
2.Explored and engaged in the game of Yahtzee utilizing GenAI techniques.
3.Developed and executed a testing strategy for the Yahtzee code, employing GenAI methodologies.
4.Created a Markov chain model for text generation.